#include "SGLib.h"

#include <time.h>

int main()
{
    int begin = GetTickCount();

    sgCreateWindow(800, 640);

    for (int i = 0; i < 800; i++) {
     	sgSetPixel(i, i - i / 50 * 50);
    }

    printf("%g", ((float)(GetTickCount() - begin)) / 1000);

    return 0;
}
