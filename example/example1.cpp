#include "../SGLib.h"

int main()
{
	sgCreateWindow(800, 640);

	sgClearColor(SG_LIGHTBLUE);

	sgSetColor(SG_LIGHTGREEN);
	sgSetFillColor(SG_LIGHTGREEN);
	sgRectangle(0, 440, 800, 640);

	sgSetColor(SG_BROWN);
	sgSetFillColor(SG_BROWN);
	sgRectangle(200, 240, 600, 440);

	POINT points[3] = {{400, 100},
	                   {160, 240},
	                   {640, 240}};

	sgSetColor(SG_RED);
	sgSetFillColor(SG_RED);
	sgPolygon(points, 3);

	sgSetColor(SG_LIGHTBLUE);
	sgSetFillColor(SG_LIGHTBLUE);
	sgRectangle(350, 280, 450, 380);

	sgSetColor(SG_BROWN);
	sgLine(400, 280, 400, 380);
	sgLine(350, 330, 450, 330);

	return 0;
}
