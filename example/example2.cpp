#include "SGLib.h"

int main()
{
	sgCreateWindow(800, 600, "Planet earth");

	sgSetColor(SG_WHITE);
	sgSetFillColor(SG_TRANSPARENT);
	sgRectangle(10, 10, 790, 590);

	sgSetColor(SG_CYAN);
	sgEllipse(400, 300, 200, 150);
	sgEllipse(400, 300, 155, 150);
	sgEllipse(400, 300, 110, 150);
	sgEllipse(400, 300, 70,  150);
	sgEllipse(400, 300, 25,  150);
	sgEllipse(400, 300, 200, 150);
	sgEllipse(400, 300, 200, 110);
	sgEllipse(400, 300, 200, 70);
	sgEllipse(400, 300, 200, 30);
	sgLine(200, 300, 600, 300);

	sgSetColor(SG_LIGHTGREEN);
	sgTextOut(320, 480, "Hello, world!\\n");

	sgSetColor(SG_YELLOW);
	sgSetFillColor(SG_YELLOW);
	sgLine(385, 135, 385, 120);
	sgLine(385, 135, 375, 150);
	sgLine(385, 135, 395, 150);
	sgLine(385, 125, 375, 135);
	sgLine(385, 125, 400, 120);
	sgCircle(385, 115, 6);

	sgSetFillColor(SG_TRANSPARENT);
	sgLine(400, 75, 400, 150);
	sgRectangle(400, 75, 450, 115);
	sgTextOut(412, 85, "C++");

	return 0;
}
