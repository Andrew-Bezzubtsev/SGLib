//{===============================================================================================================
//! \mainpage
//!
//! SGLib - ���������� ����� ������� ��� ������������ ������ Windows.
//! ���������: ��������� ����� �������� ���������� � �������� Win32.
//! ���������� �������� �� ����� C++.
//!
//! $Copyright: (C) Andrew Bezzubtsev, 2015 $
//! $Date: 12.12.2015 $
//! <img src=img/1.png>
//! <img src=img/2.png>
//!
//! \warning ��� �����-������ ����������. � ����� ������� �������� �� andrew.bezzubtsev@icloud.com
//!
//! \par ����������
//! - <a href=https://dev.windows.com/ru-ru>����� ���������� ��� Windows</a>
//! - <a href=http://ded32.net.ru/load/1-1-0-4>���������� ������ ��������� (The Dumb Artist Library, TX Library, TXLib)</a>
//================================================================================================================
//! \defgroup Debug �������
//! \defgroup Window ����
//! \defgroup Drawing ���������
//! \defgroup Mouse ����
//! \defgroup Keyboard ����������
//! \defgroup Console �������
//! \defgroup Const ���������
//! \defgroup Other ������
//}===============================================================================================================

#ifndef SGLIB_H
#define SGLIB_H

#if !defined(__cplusplus)
#error ______________________________
#error
#error SGLib error:
#error You are trying to compile
#error SGLib-based application with
#error not C++ compiler!
#error
#error ______________________________
#endif

#if !defined(WIN32) && !defined(__WIN32__) && !defined(_WIN32) && !defined(_WIN32_WINNT)
#error ______________________________
#error
#error SGLib error:
#error You are using not Windows OS
#error Use windows or wine emulator!
#error
#error ______________________________
#endif

#if defined(UNICODE) || defined(_UNICODE)
#undef UNICODE
#undef _UNICODE
#endif

#if defined(__GNUC__)
#pragma GCC diagnostic ignored "-Wmissing-field-initializers"
#pragma GCC diagnostic ignored "-Wold-style-cast"
#define __noreturn __attribute__((noreturn))
#elif defined(_MSC_VER) && (_MSC_VER >= 1400)
#pragma setlocale("russian")
#pragma comment(lib, "gdi32.lib")
#pragma warning(disable: 4646)

#define _CRT_SECURE_NO_WARNINGS 1
#define _CRT_SECURE_CPP_OVERLOAD_SECURE_NAMES 1
#define _USE_MATH_DEFINES
#define __noreturn __declspec(noreturn)
#endif

#define WINVER       0x500
#define _WIN32_WINNT WINVER
#define _WIN32_IE    WINVER

#include <windows.h>
#include <time.h>
#include <stdio.h>
#include <process.h>
#include <locale.h>
#include <math.h>
#include <float.h>
#include <vector>

//{===============================================================================================================
//! \ingroup Other
//!
//! \arg dll �������� ����������
//! \arg func �������� �������
//! \arg required ���������� �� ������
//!
//! ������� ����������� ������� func �� ������������ ���������� dll, ���� ������ �� ���������, �� ����������,
//! ����� ������ ��������� �� ������
//!
//! ������ �������������:
//! \code
//! #define DLLIMPORT(lib, retval, name, params) retval (WINAPI//! name) params = (retval (WINAPI*) params)__sg_DllImport__(lib ".dll", #name, 1)
//!
//! namespace Windows {
//!     ...
//!     DLLIMPORT("GDI32", HDC, CreateCompatibleDC, (HDC dc));
//!     ...
//! }
//! \endcode
//!
//! \see DLLIMPORT, TRY_DLLIMPORT
//}===============================================================================================================

FARPROC __sg_DllImport__(const char dll[], const char func[], int required = 1);

FARPROC __sg_DllImport__(const char dll[], const char func[], int required /*= 1*/)
{
	if (!dll || !func)
		return 0;

	if (*dll == '\0' || *func == '\0')
		return 0;

	HMODULE library = GetModuleHandle(dll);
	if (!library && dll)
		library = LoadLibrary(dll);

	if (!library && required)
		fprintf(stderr, "Can't load %s from %s!\n", func, dll);

	if (!library)
		return 0;

	FARPROC addr = GetProcAddress(library, func);
	if (!addr && required)
		fprintf(stderr, "Can't load %s from %s!\n", func, dll);

	return addr;
}

//{===============================================================================================================
//! \ingroup Other
//!
//! \arg lib �������� ����������
//! \arg retval ������������ ��������
//! \arg name �������� �������
//! \arg params ��������� �������
//!
//! �����-������� ����������� ������ retval name(params) �� ���������� lib, ������ ������ ����������.
//!
//! ������ �������������:
//! \code
//! namespace Windows {
//!     ...
//!     DLLIMPORT("GDI32", HDC, CreateCompatibleDC, (HDC dc));
//!     ...
//! }
//! \endcode
//!
//! \see TRY_DLLIMPORT, __sg_DllImport__
//}===============================================================================================================

#define DLLIMPORT(lib, retval, name, params) retval (WINAPI *name) params = (retval (WINAPI *)params)__sg_DllImport__(lib ".dll", #name, 1)

//{===============================================================================================================
//! \ingroup Other
//!
//! \arg lib �������� ����������
//! \arg retval ������������ ��������
//! \arg name �������� �������
//! \arg params ��������� �������
//!
//! �����-������� ����������� ������ retval name(params) �� ���������� lib, ������ ������ �� �����������.
//! � � ������ ������ ������� �� ���� ������� �� �����! :)
//!
//! ������ �������������:
//! \code
//! namespace Windows {
//!     ...
//!     TRY_DLLIMPORT("Kernel32", BOOL, SetConsoleFont, (HANDLE con, DWORD fontIndex));
//!     ...
//! }
//! \endcode
//!
//! \see TRY_DLLIMPORT, __sg_DllImport__
//}===============================================================================================================

#define TRY_DLLIMPORT(lib, retval, name, params) retval (WINAPI *name) params = (retval (WINAPI*) params)__sg_DllImport__(lib ".dll", #name, 0)

#define SG_ON_WINDOWFAIL sgOK() asserted; sgBuffer() asserted; if (!sgOK() || !sgBuffer())

namespace Windows {
	struct NT_CONSOLE_PROPS;
	struct CONSOLE_FONT_INFOEX;

	DLLIMPORT("GDI32",    HDC,      CreateCompatibleDC,       (HDC dc));
	DLLIMPORT("GDI32",    HBITMAP,  CreateCompatibleBitmap,   (HDC dc, int width, int height));
	DLLIMPORT("GDI32",    HGDIOBJ,  GetStockObject,           (int object));
	DLLIMPORT("GDI32",    HGDIOBJ,  SelectObject,             (HDC dc, HGDIOBJ object));
	DLLIMPORT("GDI32",    HGDIOBJ,  GetCurrentObject,         (HDC dc, unsigned objectType));
	DLLIMPORT("GDI32",    int,      GetObjectA,               (HGDIOBJ obj, int bufsize, void* buffer));
	DLLIMPORT("GDI32",    DWORD,    GetObjectType,            (HGDIOBJ object));
	DLLIMPORT("GDI32",    BOOL,     DeleteDC,                 (HDC dc));
	DLLIMPORT("GDI32",    BOOL,     DeleteObject,             (HGDIOBJ object));
	DLLIMPORT("GDI32",    COLORREF, SetTextColor,             (HDC dc, COLORREF color));
	DLLIMPORT("GDI32",    COLORREF, SetBkColor,               (HDC dc, COLORREF color));
	DLLIMPORT("GDI32",    int,      SetBkMode,                (HDC dc, int bkMode));
	DLLIMPORT("GDI32",    HFONT,    CreateFontA,              (int height, int width, int escapement, int orientation,
								   int weight, DWORD italic, DWORD underline, DWORD strikeout,
								   DWORD charSet, DWORD outputPrec, DWORD clipPrec,
								   DWORD quality, DWORD pitchAndFamily, const char face[]));
	DLLIMPORT("GDI32",    int,      EnumFontFamiliesExA,      (HDC dc, LPLOGFONT logFont, FONTENUMPROC enumProc,
								   LPARAM lParam, DWORD reserved));
	DLLIMPORT("GDI32",    COLORREF, SetPixel,                 (HDC dc, int x, int y, COLORREF color));
	DLLIMPORT("GDI32",    COLORREF, GetPixel,                 (HDC dc, int x, int y));
	DLLIMPORT("GDI32",    HPEN,     CreatePen,                (int penStyle, int width, COLORREF color));
	DLLIMPORT("GDI32",    HBRUSH,   CreateSolidBrush,         (COLORREF color));
	DLLIMPORT("GDI32",    BOOL,     MoveToEx,                 (HDC dc, int x, int y, POINT* point));
	DLLIMPORT("GDI32",    BOOL,     LineTo,                   (HDC dc, int x, int y));
	DLLIMPORT("GDI32",    BOOL,     Polygon,                  (HDC dc, const POINT points[], int count));
	DLLIMPORT("GDI32",    BOOL,     Polyline,                 (HDC dc, const POINT points[], int count));
	DLLIMPORT("GDI32",    BOOL,     Rectangle,                (HDC dc, int x0, int y0, int x1, int y1));
	DLLIMPORT("GDI32",    BOOL,     RoundRect,                (HDC dc, int x0, int y0, int x1, int y1, int sizeX, int sizeY));
	DLLIMPORT("GDI32",    BOOL,     Ellipse,                  (HDC dc, int x0, int y0, int x1, int y1));
	DLLIMPORT("GDI32",    BOOL,     Arc,                      (HDC dc, int x0, int y0, int x1, int y1,
								   int xStart, int yStart, int xEnd, int yEnd));
	DLLIMPORT("GDI32",    BOOL,     Pie,                      (HDC dc, int x0, int y0, int x1, int y1,
								   int xStart, int yStart, int xEnd, int yEnd));
	DLLIMPORT("GDI32",    BOOL,     Chord,                    (HDC dc, int x0, int y0, int x1, int y1,
								   int xStart, int yStart, int xEnd, int yEnd));
	DLLIMPORT("GDI32",    BOOL,     TextOutA,                 (HDC dc, int x, int y, const char string[], int length));
	DLLIMPORT("GDI32",    UINT,     SetTextAlign,             (HDC dc, unsigned mode));
	DLLIMPORT("GDI32",    BOOL,     GetTextExtentPoint32A,    (HDC dc, const char string[], int length, SIZE* size));
	DLLIMPORT("GDI32",    BOOL,     ExtFloodFill,             (HDC dc, int x, int y, COLORREF color, unsigned type));
	DLLIMPORT("GDI32",    BOOL,     BitBlt,                   (HDC dest, int xDest, int yDest, int width, int height,
								   HDC src,  int xSrc,  int ySrc,  DWORD rOp));
	DLLIMPORT("GDI32",    int,      SetDIBitsToDevice,        (HDC dc, int xDest, int yDest, DWORD width, DWORD height,
								   int xSrc, int ySrc, unsigned startLine, unsigned numLines,
								   const void* data, const BITMAPINFO* info, unsigned colorUse));
	DLLIMPORT("GDI32",    int,      GetDIBits,                (HDC hdc, HBITMAP hbmp, unsigned uStartScan, unsigned cScanLines,
								   void* lpvBits, BITMAPINFO* lpbi, unsigned usage));
	DLLIMPORT("GDI32",    BOOL,     PatBlt,                   (HDC dc, int x0, int y0, int width, int height, DWORD rOp));
	DLLIMPORT("GDI32",    int,      SetROP2,                  (HDC dc, int mode));
	DLLIMPORT("GDI32",    HRGN,     CreateRectRgn,            (int x0, int y0, int x1, int y1));
	DLLIMPORT("GDI32",    BOOL,     GetBitmapDimensionEx,     (HBITMAP bitmap, SIZE* dimensions));
	DLLIMPORT("User32",   int,      DrawTextA,                (HDC dc, const char text[], int length, RECT* rect, unsigned format));
	DLLIMPORT("User32",   HANDLE,   LoadImageA,               (HINSTANCE inst, const char name[], unsigned type,
								   int sizex, int sizey, unsigned mode));
	DLLIMPORT("WinMM",    BOOL,     PlaySound,                (const char sound[], HMODULE mod, DWORD mode));
	DLLIMPORT("Kernel32", HWND,     GetConsoleWindow,         (void));
	DLLIMPORT("OLE32",    HRESULT,  CoInitialize,             (void*));
	DLLIMPORT("OLE32",    HRESULT,  CoCreateInstance,         (REFCLSID clsid, LPUNKNOWN, DWORD, REFIID iid, PVOID* value));
	DLLIMPORT("OLE32",    void,     CoUninitialize,           (void));
	DLLIMPORT("Shell32",  HINSTANCE, ShellExecuteA,            (HWND wnd, const char operation[], const char file[],
								   const char parameters[], const char directory[], int showCmd));

	TRY_DLLIMPORT("NTDLL",    char*,    wine_get_version,          (void));
	TRY_DLLIMPORT("MSVCRT",   void,     _cexit,                    (void));
	TRY_DLLIMPORT("Kernel32", BOOL,     SetConsoleFont,            (HANDLE con, DWORD fontIndex));
	TRY_DLLIMPORT("Kernel32", BOOL,     GetConsoleFontInfo,        (HANDLE con, bool fullScr, DWORD nFonts, CONSOLE_FONT_INFO* info));
	TRY_DLLIMPORT("Kernel32", DWORD,    GetNumberOfConsoleFonts,   (void));
	TRY_DLLIMPORT("Kernel32", BOOL,     GetCurrentConsoleFont,     (HANDLE con, bool maxWnd, CONSOLE_FONT_INFO*   curFont));
	TRY_DLLIMPORT("Kernel32", BOOL,     GetCurrentConsoleFontEx,   (HANDLE con, bool maxWnd, CONSOLE_FONT_INFOEX* curFont));
	TRY_DLLIMPORT("Kernel32", BOOL,     SetCurrentConsoleFontEx,   (HANDLE con, bool maxWnd, CONSOLE_FONT_INFOEX* curFont));
	TRY_DLLIMPORT("MSImg32",  BOOL,     TransparentBlt,            (HDC dest, int destX, int destY, int destWidth, int destHeight,
								        HDC src,  int srcX,  int srcY,  int srcWidth,  int srcHeight,
								        unsigned transparentColor));
	TRY_DLLIMPORT("MSImg32",  BOOL,     AlphaBlend,                (HDC dest, int destX, int destY, int destWidth, int destHeight,
								        HDC src,  int srcX,  int srcY,  int srcWidth,  int srcHeight,
								        BLENDFUNCTION blending));
	TRY_DLLIMPORT("User32",   BOOL,     IsHungAppWindow,           (HWND wnd));
	TRY_DLLIMPORT("User32",   HWND,     GhostWindowFromHungWindow, (HWND wnd));
}

//{===============================================================================================================
//! \ingroup Const
//}===============================================================================================================

//! ����� ������, ���� �������� �����, �������������� ������...
#define BUFFER_DRAW    0
//! ����� ������, ���� �������� ������
#define BUFFER_CON     1
//! ������������ ����� �������� ��� ������������� ����������
#define SG_TIMEOUT  1500

#if defined(__GNUC__)
#define __SG_FUNCTION__ __PRETTY_FUNCTION__
#elif defined(_MSC_VER) || defined(__INTEL_COMPILER)
#define __SG_FUNCTION__ __FUNCTION__
#elif defined(__BORLANDC__)
#define __SG_FUNCTION__ __FUNC__
#elif defined(__STDC_VERSION__)
#define __SG_FUNCTION__ __func__
#endif

#define __SG_FILE__ __FILE__
#define __SG_LINE__ __LINE__

//{===============================================================================================================
//! \ingroup Debug
//!
//! \arg expr ������� ��� ��������
//!
//! ���� ������� expr ������� ��������� ��������� �� ������ � ����������� ������������� ���������� ���������, �����
//! ���� ���������� ������� abort().
//!
//! ������ �������������:
//! \code
//! #include "SGLib.h"
//!
//! int main()
//! {
//!     int expr = 0;
//!     assert(expr);
//!
//!     if (expr == 1)
//!         printf("Welcome to programmer's hell!\nNow, please debug my library! :)\n");
//!
//!     return 0;
//! }
//! \endcode
//!
//! \see asserted
//}===============================================================================================================

#ifndef assert
#define assert(expr) if (!expr) {__sg_HandlingError__(__SG_FILE__, __SG_FUNCTION__, __SG_LINE__);}
#endif // assert

//{===============================================================================================================
//! \ingroup Debug
//!
//! ���� ������� ����� ������� ��������� ��������� �� ������ � ����������� ������������� ���������� ���������, �����
//! ���� ���������� ������� abort().
//!
//! ������ �������������:
//! \code
//! #include "SGLib.h"
//!
//! int main()
//! {
//!     int expr = 0;
//!     expr asserted;
//!
//!     if (expr == 1)
//!         printf("Welcome to programmer's hell!\nNow, please debug my library! :)\n");
//!
//!     return 0;
//! }
//! \endcode
//!
//! ����������� ������ assert-�.
//!
//! \see assert(expr)
//}===============================================================================================================

#ifndef asserted
#define asserted || __sg_HandlingError__(__SG_FILE__, __SG_FUNCTION__, __SG_LINE__);
#endif // asserted

#ifndef sgDraw
#define sgDraw(func) (sgUnlock((sgLock(), func)))
#endif // sgDraw

#ifndef ArraySize
#define ArraySize(arr) (sizeof(arr) / sizeof(*arr))
#endif // ArraySize

//{===============================================================================================================
//! \ingroup Const
//!
//! �����-���������, �������� ���������� �������� � �������.
//!
//! ������ �������������:
//! \code
//! #include "SGLib.h"
//!
//! int main()
//! {
//!     printf("1 radian = %f degrees\n", RADIAN);
//!     return 0;
//! }
//! \endcode
//}===============================================================================================================

#ifndef RADIAN
	#define RADIAN ((float)(57.295779513))
#endif // radian

//{===============================================================================================================
//! \ingroup Other
//!
//! \arg a ����� ��� ����������
//!
//! ��������� ����� a
//!
//! ������� �������������:
//! \code
//! #include "SGLib.h"
//!
//! int main()
//! {
//!     printf("Magic number %f is nearly %d.\n", M_PI, ROUND(M_PI));
//!     return 0;
//! }
//! \endcode
//}===============================================================================================================

#ifndef ROUND
	#define ROUND(a) (int)(a + 0.5)
#endif // ROUND

namespace __SGLib__ {
	namespace __window__ {
		volatile int width    = 0;
		volatile int height   = 0;
		const char *title  = 0;

		HWND descriptor = 0;
		int created = 0;
		HANDLE proc = 0;

		DWORD style = WS_POPUP | WS_SYSMENU | WS_BORDER | WS_CAPTION;
		CRITICAL_SECTION cs = CRITICAL_SECTION{};

		HDC buffer[2] = {0};

		int timer = 0;
	}

	namespace __kb__ {
		int keys[256] = {0};
	}

	namespace __mouse__ {
		int buttons = 0;
		short X = 0;
		short Y = 0;
		long wheel = 0;
	}

	namespace __user__ {
		int freeze = 0;
		int cursor = 1;

		int button_id = 0;
		std::vector <void (*)()> on_push;
	}
}

//{===============================================================================================================
//! \ingroup Const
//! ��������� ����� ��� ���������...
//}===============================================================================================================

#define CLR_NONE CLR_INVALID

enum sgColors {
	//! ������ ����
	SG_BLACK      = 0x000000,
	//! ����� ����
	SG_BLUE       = 0x800000,
	//! ������� ����
	SG_LIGHTBLUE  = 0xFF0000,
	//! �����-����� ����
	SG_DARKBLUE   = 0x400000,
	//! ������� ����
	SG_GREEN      = 0x008000,
	//! ������-������� ����
	SG_LIGHTGREEN = 0x00FF00,
	//! �����-������� ����
	SG_DARKGREEN  = 0x004000,
	//! ������� ����
	SG_RED        = 0x000080,
	//! ������-������� ����
	SG_LIGHTRED   = 0x0000FF,
	//! �����-������� ����
	SG_DARKRED    = 0x000040,
	//! ��������� ����
	SG_CYAN       = 0x808000,
	//! ��������� ����
	SG_MAGENTA    = 0x800080,
	//! ������ ����
	SG_BROWN      = 0x003E7D,
	//! ���������(�� ����� ���� ����� ��� �����)
	SG_ORANGE     = 0x0080FF,
	//! ����� ����
	SG_GRAY       = 0xA0A0A0,
	//! ������-����� ����
	SG_LIGHTGRAY  = 0xC0C0C0,
	//! ������ ����
	SG_YELLOW     = 0x80FFFF,
	//! ����� ����
	SG_WHITE      = 0xFFFFFF,
	//! ��������� ���������� ����
	SG_TRANSPARENT = CLR_NONE,
	//! ��������� ���������� ����
	SG_NULL        = SG_TRANSPARENT,
	//! �������� ��� � ������ HSL
	SG_HUE         = 0x04000000,
	//! ������������ � ������ HSL
	SG_SATURATION  = 0x05000000,
	//! �������� � ������ HSL
	SG_LIGHTNESS   = 0x06000000
};

#if defined(__GNUC__)
#define __noreturn __attribute__((noreturn))
#elif defined(_MSC_VER)
#define __noreturn __declspec(noreturn)
#endif

inline __noreturn int __sg_HandlingError__(const char *file, const char *func, int line);
void __sgCanvas_CreateWindow__();
inline void __sgCanvas_HandleWindow__();
LRESULT CALLBACK __sgCanvas_ProcessMessages__(HWND window, UINT msg, WPARAM wParam, LPARAM lParam);

void __sgCanvas_OnCreate__(HWND window);
inline void __sgCanvas_OnTimer__(HWND window);
inline void __sgCanvas_OnPaint__(HWND window);
void __sgCanvas_OnKB__(UINT msg, WPARAM wParam, LPARAM lParam);
void __sgCanvas_OnChar__(WPARAM wParam, LPARAM lParam);
void __sgCanvas_OnMouse__(UINT msg, WPARAM wParam, LPARAM lParam);
void __sgCanvas_OnDestroy__();

void __sgCleanup__();
inline void __sgExit__();

inline HDC __sgGenBuffer__(HWND window);
void __sgConsole_Draw__();

//{===============================================================================================================
//! \ingroup Window
//!
//! ���������, � ������� �� ��������� ����.
//! ����, �� ���������� - 1.
//! ����� - 0.
//!
//! ������ �������������:
//! \code
//! sgOK() asserted;
//! \endcode
//!
//! \return ��������� ��������
//!
//! \see sgCreateWindow(int width, int height, const char title[] = ""), sgWindow()
//}===============================================================================================================

inline int sgOK();

//{===============================================================================================================
//! \ingroup Window
//!
//! \arg width ������ ������� ���������
//! \arg height ������ ������� ���������
//! \arg title ��������� ���� (�� ��������� - �������� ������������ �����)
//!
//! ������� ���� � �������� ��������� [width]x[height] ��������, � ���������� title.
//! ���� ����������� � ����� ������, ���������� - ������.
//! ��� ����, ����� �������� ����� ����, ����� �������� � ����� __SGLib__::__window__::style �����.
//! ��������� �����:
//! WS_BORDER - � ���� ���� ������ �������.
//! WS_CAPTION - � ���� ���� ���������(�������� WS_BORDER).
//! WS_CHILD - ���� ��������, �� ����� ����� ���� � ������������ � WS_POPUP
//! WS_CHILDWINDOW - ��. WS_CHILD.
//! WS_CLIPCHILDREN - ��������� �������, ������� ��������� �������, ����� ������� ���������� � ������������ ����.
//! ���� ����� ������������ ��� �������� ������������� ����.
//! WS_DISABLED - ���� ���������, �� ����� �������������� ��� �����.
//! WS_DLGFRAME - ���� ����� ������ �������, ��� �������, ������������ � ����������� ������.
//! ���� � ���� ������ �� ����� ����� ������ ���������.
//! WS_HSCROLL - ����� �������������� ������� ��������.
//! WS_ICONIC - ��. WS_MINIMISE.
//! WS_MAXIMIZE - ���� ��������� ���������.
//! WS_MAXIMIZEBOX - ���� ����� ������ ��� ���������.
//! WS_MINIMISE - ���� ��������� ���������.
//! WS_MINIMISEBOX - ���� ����� ������ ��� �������.
//! WS_OVERLAPPED - ���� ������������� ����. ������������� ���� ����� ������ ��������� � �������.
//! WS_OVERLAPPEDWINDOW - WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_THICKFRAME | WS_MINIMIZEBOX | WS_MAXIMIZEBOX.
//! WS_POPUP - ����������� ����. ���� ����� �� ����� ���� ����������� �� ������ WS_CHILD.
//! WS_POPUPWINDOW - ����������� ����. � WS_CAPTION � WS_POPUPWINDOW ����� ������ ���� ����������.
//! ����� ������� ���� ���� �������.
//! WS_SIZEBOX - ��. WS_THIKFRAME.
//! WS_SYSMENU - ����� ���� ���� �� ������ ���������. ����� WS_CAPTION ����� ������ ���� ������.
//! WS_THICKFRAME - ����� ������� ��� �������� �������� ����.
//! WS_TILED - ��. WS_OVERLAPPED.
//! WS_TILEDWINDOW - ��. WS_OVERLAPPEDWINDOW.
//! WS_VISIBLE - ������������� ������� ����.
//! WS_VSCROLL - ����� ������������ ������� ���������
//!
//! ������ �������������:
//! \code
//! #include "SGLib.h"
//!
//! int main()
//! {
//!     sgCreateWindow(800, 640);
//!     printf("����������, �� ���������� �������!\n");
//!
//!     return 0;
//! }
//! \endcode
//!
//! \see sgWindow(), sgGetExtent(), sgGetExtentX(), sgGetExtentY(), sgOK()
//}===============================================================================================================

inline void sgCreateWindow(int width, int height, const char title[] = "");

//{===============================================================================================================
//! \ingroup Window
//!
//! ������� ���������� ������ �� ������������� ����� ��������� ����.
//!
//! ������ �������������:
//! \code
//! inline void sgSetPixel(int x, int y, COLORREF color)
//! {
//!     sgOK() asserted;
//!     sgBuffer() asserted;
//!
//!     sgDraw(SetPixel(sgBuffer(), x, y, color));
//! }
//! \endcode
//!
//! \return ������ �� �������� ���������� ��������� ����.
//!
//! \see sgWindow(), sgCreateWindow(int width, int height, const char title[] = "")
//}===============================================================================================================

inline HDC& sgBuffer();

//{===============================================================================================================
//! \ingroup Window
//!
//! ������� ���������� ������ �� ���������� ����.
//!
//! ������ �������������:
//! \code
//! inline int sgOK()
//! {
//!     return __SGLib__::__window__::created && sgWindow();
//! }
//! \endcode
//!
//! \return ������ �� ���������� ����
//!
//! \see sgCreateWindow(), sgOK()
//}===============================================================================================================

inline HWND& sgWindow();

//{===============================================================================================================
//! \ingroup Window
//!
//! ���������� ������� ������� ���������, ��� ��������� SIZE.
//!
//! ������ �������������:
//! inline int sgGetExtentX()
//! {
//!     return sgGetExtent().cx;
//! }
//!
//! inline int sgGetExtentY()
//! {
//!     return sgGetExtent().cy;
//! }
//! \endcode
//!
//! ��� ��������� ������ - sgGetExtent().cx ��� sgGetExtentX().
//! ��� ��������� ������ - sgGetExtent().cy ��� sgGetExtentY().
//!
//! \return ������� ������� ��������� ���� � ���� ��������� SIZE
//!
//! \see sgGetExtentX(), sgGetExtentY()
//}===============================================================================================================

inline SIZE sgGetExtent();

//{===============================================================================================================
//! \ingroup Window
//!
//! ���������� ������ ������� ���������.
//!
//! ������ �������������:
//! \code
//! #include "SGLib.h"
//!
//! int main()
//! {
//!     sgCreateWindow(800, 640);
//!
//!     printf("%d %d\n", 800, 640);
//!     printf("%d %d\n", sgGetExtentX(), sgGetExtentY());
//!     printf("���� ����� ���� ��� ������ �����������, �� ���-�� ������ �������...\n������, ����� ������� �� ����!\n");
//!
//!     return 0;
//! }
//! \endcode
//!
//! \return ������ ������� ���������
//!
//! \see sgGetExtent(), sgGetExtentY()
//}===============================================================================================================

inline int sgGetExtentX();

//{===============================================================================================================
//! \ingroup Window
//!
//! ���������� ������ ������� ���������.
//!
//! ������ �������������:
//! \code
//! #include "SGLib.h"
//!
//! int main()
//! {
//!     sgCreateWindow(800, 640);
//!
//!     printf("%d %d\n", 800, 640);
//!     printf("%d %d\n", sgGetExtentX(), sgGetExtentY());
//!     printf("���� ����� ���� ��� ������ �����������, �� ���-�� ������ �������...\n������, ����� ������� �� ����!\n");
//!
//!     return 0;
//! }
//! \endcode
//!
//! \return ������ ������� ���������
//!
//! \see sgGetExtent(), sgGetExtentX()
//}===============================================================================================================

inline int sgGetExtentY();

//{===============================================================================================================
//! \ingroup Mouse
//!
//! ���������� ������� ����� ������������ ���� � ���� ��������� POINT
//!
//! ������ �������������:
//! \code
//! inline int sgMouseX()
//! {
//!     return sgMousePos().x;
//! }
//!
//! inline int sgMouseY()
//! {
//!     return sgMousePos().y;
//! }
//! \endcode
//!
//! \return ������� ����� ������������ ����, ��� �������� POINT
//!
//! \see sgMouseX(), sgMouseY()
//}===============================================================================================================

inline POINT sgMousePos();

//{===============================================================================================================
//! \ingroup Mouse
//!
//! ���������� ������� ����� �� ��� X ������������ ����.
//!
//! ������ �������������:
//! \code
//! #include "SGLib.h"
//!
//! int main()
//! {
//!     sgCreateWindow(800, 640);
//!
//!     if (sgMouseX() > sgGetExtentX() / 2)
//!         printf("������ ������� ����� � ������ �������� ����!\n");
//!
//!     return 0;
//! }
//! \endcode
//!
//! \return ������� ����� �� ��� X ������������ ����
//!
//! \see sgMousePos(), sgMouseY()
//}===============================================================================================================

inline int sgMouseX();

//{===============================================================================================================
//! \ingroup Mouse
//!
//! ���������� ������� ����� �� ��� Y ������������ ����.
//!
//! ������ �������������:
//! \code
//! #include "SGLib.h"
//!
//! int main()
//! {
//!     sgCreateWindow(800, 640);
//!
//!     if (sgMouseY() > sgGetExtentY() / 2)
//!         printf("������ ������� ����� � ������ �������� ����!\n");
//!
//!     return 0;
//! }
//! \endcode
//!
//! \return ������� ����� �� ��� Y ������������ ����
//!
//! \see sgMousePos(), sgMouseX()
//}===============================================================================================================

inline int sgMouseY();

//{===============================================================================================================
//! \ingroup Mouse
//!
//! ����������, �� ������� ���������� �������� ����� ����� �������� ���� �� �������� �������.
//!
//! ������ �������������:
//! \code
//! #include "SGLib.h"
//!
//! int main()
//! {
//!     sgCreateWindow(800, 640);
//!
//!     Sleep(10000); // delay for 10 seconds
//!
//!     printf("�������� ���������� �� %d!", sgMouseWheel());
//! }
//! \endcode
//!
//! \return �� ������� ���������� �������� ����� ����� �������� ���� �� �������� �������
//}===============================================================================================================

inline int sgMouseWheel();

//{===============================================================================================================
//! \ingroup Mouse
//!
//! ����������, ����� ������ ����� ������.
//! ����� ������, ����� ������ ����� ��������� �� �����.
//!
//! ��������� �����:
//! MK_LBUTTON - ����� ������ �����
//! MK_MBUTTON - ������� ������ �����
//! MK_RBUTTON - ������ ������ �����
//!
//! ������ �������������:
//! \code
//! #include "SGLib.h"
//!
//! int main()
//! {
//!     sgCreateWindow(800, 640);
//!
//!     while (!(sgMouseButtons() & MK_RBUTTON));
//!     printf("��������� ��������, �� ����� ������ ������ �����!");
//!
//!     return 0;
//! }
//! \endcode
//!
//! \return ����� ������ ����� ������
//}===============================================================================================================

inline int sgMouseButtons();

//{===============================================================================================================
//! \ingroup Drawing
//!
//! \arg color ����
//! \arg thikness ������� ������� � ��������
//!
//! ������� ������������� ���� � ������� �������, � ����� ���� ������.
//! ���������� ���� ������� - �����, � ������� - 1 �������.
//!
//! ������ �������������:
//! \code
//! #include "SGLib.h"
//!
//! int main()
//! {
//!     sgCreateWindow(800, 640);
//!     sgClearColor(SG_GREEN);
//!
//!     sgSetColor(SG_BLACK);
//!     sgLine(0, 0, 800, 640);
//!     printf("��� - ������ �����!\n");
//!
//!     Sleep(2000);
//!     sgClearConsole();
//!     sgSetColor(SG_WHITE);
//!     sgLine(800, 0, 0, 640);
//!     printf("� ��� - �����!");
//!
//!     return 0;
//! }
//! \endcode
//!
//! \see sgGetColor(), sgSetFillColor(COLORREF color), sgGetFillColor()
//}===============================================================================================================

inline void sgSetColor(COLORREF color, int thikness = 1);

//{===============================================================================================================
//! \ingroup Drawing
//!
//! ���������� ������� ���� ��������� �������.
//!
//! ������ �������������:
//! \code
//! #include "SGLib.h"
//!
//! #define RGBR(color) (unsigned char)(color)
//! #define RGBG(color) (unsigned char)((color >> 8) & 0xFF)
//! #define RGBB(color) (unsigned char)((color >> 16) & 0xFF)
//!
//! int main()
//! {
//!     sgSetColor(SG_MAGENTA);
//!     printf("Magenta contains %d red, %d green and %d blue!\n", RGBR(sgGetColor()), RGBG(sgGetColor()), RGBB(sgGetColor()));
//!     return 0;
//! }
//! \endcode
//!
//! \return ������� ���� ��������� �������
//!
//! \see sgSetColor(COLORREF color, int thikness = 1), sgSetFillColor(COLORREF color), sgGetFillColor()
//}===============================================================================================================

inline COLORREF sgGetColor();

//{===============================================================================================================
//! \ingroup Drawing
//!
//! \arg color ����
//!
//! ������������� ���� ������� �����.
//!
//! ������ �������������:
//! \code
//! #include "SGLib.h"
//!
//! int main()
//! {
//!     sgCreateWindow(800, 600, "Planet earth");
//!     sgCursor();
//!
//!     sgSetColor(SG_WHITE);
//!     sgSetFillColor(SG_TRANSPARENT);
//!     sgRectangle(10, 10, 790, 590);
//!
//!     sgSetColor(SG_CYAN);
//!     sgEllipse(400, 300, 200, 150);
//!     sgEllipse(400, 300, 155, 150);
//!     sgEllipse(400, 300, 110, 150);
//!     sgEllipse(400, 300, 70,  150);
//!     sgEllipse(400, 300, 25,  150);
//!     sgEllipse(400, 300, 200, 150);
//!     sgEllipse(400, 300, 200, 110);
//!     sgEllipse(400, 300, 200, 70);
//!     sgEllipse(400, 300, 200, 30);
//!     sgLine(200, 300, 600, 300);
//!
//!     sgSetColor(SG_LIGHTGREEN);
//!     sgTextOut(320, 480, "Hello, world!\\n");
//!
//!     sgSetColor(SG_YELLOW);
//!     sgSetFillColor(SG_YELLOW);
//!     sgLine(385, 135, 385, 120);
//!     sgLine(385, 135, 375, 150);
//!     sgLine(385, 135, 395, 150);
//!     sgLine(385, 125, 375, 135);
//!     sgLine(385, 125, 400, 120);
//!     sgCircle(385, 115, 6);
//!
//!     sgSetFillColor(SG_TRANSPARENT);
//!     sgLine(400, 75, 400, 150);
//!     sgRectangle(400, 75, 450, 115);
//!     sgTextOut(412, 85, "C++");
//!
//!     return 0;
//! }
//!
//! \endcode
//!
//! \see sgGetFillColor(), sgSetColor(COLORREF color, int thikness = 1)
//}===============================================================================================================

inline void sgSetFillColor(COLORREF color);

//{===============================================================================================================
//! \ingroup Drawing
//!
//! ���������� ������� ���� �������.
//!
//! ������ �������������:
//! \code
//! #include "SGLib.h"
//!
//! int main()
//! {
//!     ...
//!     // �����������, �� ������� ���� ���������� ����, �� �� ������� ������� � ��� ���� ������
//!     COLORREF color = sgGetFillColor();
//!     ...
//!
//!     return 0;
//! }
//! \endcode
//!
//! \return ������� ���� ������� �����
//!
//! \see sgSetFillColor(COLORREF color), sgSetColor(COLORREF color, int thikness = 1), sgGetColor()
//}===============================================================================================================

inline COLORREF sgGetFillColor();

//{===============================================================================================================
//! \ingroup Drawing
//!
//! \arg color ����
//!
//! ������� ���� ��������� ������ ������ color.
//!
//! ������ �������������:
//! \code
//! // ����������� ��� ����� ����� ������!
//!
//! #include "SGLib.h"
//!
//! int main()
//! {
//!     sgCreateWindow(800, 640);
//!     sgClearColor(SG_WHITE);
//!
//!     ...
//!
//!     return 0;
//! }
//! \endcode
//!
//! \see sgGetFillColor(), sgGetColor(), sgSetColor(COLORREF color, int thikness = 1), sgSetFillColor(COLORREF color)
//}===============================================================================================================

inline void sgClearColor(COLORREF color = sgGetFillColor());

//{===============================================================================================================
//! \ingroup Drawing
//!
//! \arg x ������� ������� �� ��� X
//! \arg y ������� ������� �� ��� Y
//! \arg color ����(�� ��������� - ������� ���� �������)
//!
//! ������� �������������:
//! \code
//! #include "SGLib.h"
//!
//! int main()
//! {
//! 	sgCreateWindow(800, 640);
//!
//!     for (int i = 0; i < 640; i++)
//!     	sgSetPixel(sqrt(i), i);
//!
//! 	return 0;
//! }
//! \endcode
//}===============================================================================================================

inline void sgSetPixel(int x, int y, COLORREF color = sgGetColor());
inline COLORREF sgGetPixel(int x, int y);
inline void sgLine(int x0, int y0, int x1, int y1);
inline void sgRectangle(int x0, int y0, int x1, int y1);
inline void sgEllipse(int x, int y, int x_rad, int y_rad);
inline void sgCircle(int x, int y, int rad);
inline void sgRoundRect(int x, int y, int x1, int y1, int dx, int dy);
inline void sgArc(int x0, int y0, int x1, int y1, int startAngle, int totalAngle);
inline void sgPie(int x0, int y0, int x1, int y1, int startAngle, int totalAngle);
inline void sgChord(int x0, int y0, int x1, int y1, int startAngle, int totalAngle);
inline void sgPolygon(POINT *vertices, int num_vertices = ~0);
inline void sgPolyline(POINT *vertices, int num_vertices = ~0);
inline void sgTextOut(int x, int y, char text[]);

inline void sgSetConsoleAttribute(int attr);
inline void sgClearConsole();
inline void sgCursor(int val = 0);

inline int sgLock(bool force = true);
inline void sgUnlock();

template<typename TYPE>
inline TYPE sgUnlock(TYPE val);

inline __noreturn int __sg_HandlingError__(const char *file, const char *func, int line)
{
	char msg[2048] = "";
	sprintf(msg, "Error!\nExcepted an error!\nIn file %s, on line %d\n%s function.\nApplication can be executed not corrected.", file, line, func);

	MessageBox(sgWindow(), msg, "SGLib error!", MB_ICONSTOP);
	exit(EXIT_FAILURE);
}

void __sgCanvas_CreateWindow__()
{
	!sgOK() asserted;

	char class_name[1024] = "";
	char window_title[1024] = "";

	strcpy(window_title, __SGLib__::__window__::title);
	if (*window_title == '\0')
		GetModuleFileName(0, window_title, sizeof(window_title) - 1);

	sprintf(class_name, "|--------------------------------------------------SGLib v2.0 window: %s--------------------------------------------------|", window_title);

	WNDCLASS wcls = {sizeof(wcls)};
	wcls.style = CS_OWNDC | CS_HREDRAW | CS_VREDRAW;
	wcls.lpfnWndProc = __sgCanvas_ProcessMessages__;
	wcls.hIcon = LoadIcon(0, IDI_APPLICATION);
	wcls.hCursor = LoadCursor(0, IDC_ARROW);
	wcls.hbrBackground = (HBRUSH)Windows::GetStockObject(BLACK_BRUSH);
	wcls.lpszClassName = class_name;

	RegisterClass(&wcls) asserted;

	RECT rect = {0, 0, __SGLib__::__window__::width, __SGLib__::__window__::height};
	AdjustWindowRect(&rect, __SGLib__::__window__::style, 0);

	SIZE screen = {GetSystemMetrics(SM_CXSCREEN), GetSystemMetrics(SM_CYSCREEN)};
	SIZE size = {rect.right - rect.left, rect.bottom - rect.top};
	POINT pos = {(screen.cx - size.cx) / 2, (screen.cy - size.cy) / 2};

	__SGLib__::__window__::descriptor = CreateWindow(class_name, window_title, __SGLib__::__window__::style,
						         pos.x, pos.y, size.cx, size.cy, HWND_DESKTOP, 0,
						         GetModuleHandle(0), 0);

	__SGLib__::__window__::created = 1;

	ShowWindow(sgWindow(), SW_SHOW);
	ShowWindow(GetConsoleWindow(), SW_HIDE);

	__sgCanvas_HandleWindow__();
}

void __sgCanvas_HandleWindow__()
{
	MSG message = { };

	while (GetMessage(&message, 0, 0, 0)) {
		TranslateMessage(&message);
		DispatchMessage(&message);
	}
}

LRESULT CALLBACK __sgCanvas_ProcessMessages__(HWND window, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg) {
		case WM_CREATE:
			__sgCanvas_OnCreate__(window);
			break;

		case WM_TIMER:
			__sgCanvas_OnTimer__(window);
			break;

		case WM_PAINT:
			__sgCanvas_OnPaint__(window);
			break;

		case WM_KEYDOWN:
		case WM_KEYUP:
			__sgCanvas_OnKB__(msg, wParam, lParam);
			break;

		case WM_CHAR:
			__sgCanvas_OnChar__(wParam, lParam);
			break;

		case WM_LBUTTONDOWN:
		case WM_LBUTTONUP:
		case WM_MBUTTONDOWN:
		case WM_MBUTTONUP:
		case WM_RBUTTONDOWN:
		case WM_RBUTTONUP:
		case WM_MOUSEWHEEL:
		case WM_MOUSEMOVE:
			__sgCanvas_OnMouse__(msg, wParam, lParam);
			break;

		case WM_DESTROY:
			__sgCanvas_OnDestroy__();
			break;

		case WM_QUIT:
			__sgExit__();
			break;

		default:
			break;
	}

	return DefWindowProc(window, msg, wParam, lParam);
}

void __sgCanvas_OnCreate__(HWND window)
{
	setlocale(LC_ALL, "Russian");

	window asserted;

	__SGLib__::__window__::descriptor = window;

	__SGLib__::__window__::buffer[BUFFER_DRAW] = __sgGenBuffer__(window);
	__SGLib__::__window__::buffer[BUFFER_CON]  = __sgGenBuffer__(window);

	SetTimer(window, __SGLib__::__window__::timer, 0, 0) asserted;

	Windows::SelectObject(sgBuffer(), Windows::GetStockObject(WHITE_PEN)) asserted;
	Windows::SelectObject(sgBuffer(), Windows::GetStockObject(NULL_BRUSH)) asserted;

	RECT rect = {0};

	GetClientRect(sgWindow(), &rect);
	SIZE canvas = {rect.right - rect.left, rect.bottom - rect.top};

	HANDLE out = GetStdHandle(STD_OUTPUT_HANDLE);

	CONSOLE_SCREEN_BUFFER_INFO console_data = {{80, 25}, {0}, 0, {0, 0, 80 - 1, 25 - 1}, {80, 25}};
	GetConsoleScreenBufferInfo(out, &console_data);

	SIZE FontSize = {(short)(console_data.srWindow.Right  - console_data.srWindow.Left + 1),
		         (short)(console_data.srWindow.Bottom - console_data.srWindow.Top  + 1)};

	HFONT font = Windows::CreateFont(canvas.cy / FontSize.cy, canvas.cx / FontSize.cx,
					 0, 0, FW_REGULAR, 0, 0, 0,
					 RUSSIAN_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS,
					 DEFAULT_QUALITY, DEFAULT_PITCH, "Lucida Console");

	Windows::SelectObject(sgBuffer(), font) asserted;
	Windows::SelectObject(__SGLib__::__window__::buffer[BUFFER_CON], font) asserted;

	!Windows::SetTextColor(sgBuffer(), RGB(0, 0xCE, 0)) asserted;
    !Windows::SetTextColor(__SGLib__::__window__::buffer[BUFFER_CON], RGB(0, 0xCE, 0)) asserted;
	SetConsoleTextAttribute(out, FOREGROUND_GREEN);

	Windows::SetBkMode(sgBuffer(), TRANSPARENT) asserted;
	Windows::SetROP2(sgBuffer(), R2_COPYPEN) asserted;
	InitializeCriticalSection(&__SGLib__::__window__::cs);

	if (console_data.dwCursorPosition.X)
		putchar('\n');

	short y_delta = (short)(console_data.dwCursorPosition.Y - console_data.srWindow.Top);

	console_data.srWindow.Top = (short)(console_data.srWindow.Top + y_delta);
	console_data.srWindow.Bottom = (short)(console_data.srWindow.Bottom + y_delta);

	SMALL_RECT source  = {0, 0, (short)(console_data.dwSize.X - 1), (short)(console_data.dwSize.Y - 1)};
	CHAR_INFO  fill = {{' '}, 7};
	COORD      destination = {0, (short)-y_delta};

	console_data.dwCursorPosition.X = 0;
	console_data.dwCursorPosition.Y = (short)(console_data.dwCursorPosition.Y - y_delta);

	(console_data.srWindow.Bottom < console_data.dwSize.Y && SetConsoleWindowInfo(out, 1, &console_data.srWindow))
	||
	(ScrollConsoleScreenBuffer(out, &source, 0, destination, &fill), SetConsoleCursorPosition(out, console_data.dwCursorPosition));
}

void __sgCanvas_OnTimer__(HWND window)
{
	SG_ON_WINDOWFAIL
		return;

	InvalidateRect(window, 0, 0);
	UpdateWindow(window);
}

void __sgCanvas_OnPaint__(HWND window)
{
	SG_ON_WINDOWFAIL
		return;

	PAINTSTRUCT ps = {};
	HDC draw = BeginPaint(window, &ps);
	if (!draw)
		return;

	RECT rect = {0};
	GetClientRect(window, &rect) asserted;
	SIZE size = {rect.right - rect.left, rect.bottom - rect.top};

	if (!__SGLib__::__user__::freeze) {
		Windows::BitBlt(__SGLib__::__window__::buffer[BUFFER_CON], 0, 0, size.cx, size.cy, sgBuffer(), 0, 0, SRCCOPY);
		__sgConsole_Draw__();
		Windows::BitBlt(draw, 0, 0, size.cx, size.cy, __SGLib__::__window__::buffer[BUFFER_CON], 0, 0, SRCCOPY);
	}

	EndPaint(window, &ps) asserted
}

void __sgCanvas_OnKB__(UINT msg, WPARAM wParam, LPARAM lParam)
{
	SG_ON_WINDOWFAIL
		return;

	switch (msg) {
		case WM_KEYDOWN: {
			INPUT_RECORD evt = {0};

			evt.EventType                        = KEY_EVENT;
			evt.Event.KeyEvent.bKeyDown          = true;
			evt.Event.KeyEvent.wRepeatCount      = 1;
			evt.Event.KeyEvent.uChar.AsciiChar   = (unsigned char)MapVirtualKey((WORD)wParam, 2);
			evt.Event.KeyEvent.wVirtualScanCode  = (unsigned char)(lParam >> 16);
			evt.Event.KeyEvent.wVirtualKeyCode   = (WORD)wParam;
			evt.Event.KeyEvent.dwControlKeyState = (lParam & (1 << 24))?(ENHANCED_KEY):(0);

			if (evt.Event.KeyEvent.uChar.AsciiChar) return;

			DWORD written = 0;
			WriteConsoleInput(GetStdHandle (STD_INPUT_HANDLE), &evt, 1, &written);

			__SGLib__::__kb__::keys[wParam] = 1;
			break;
		}

		case WM_KEYUP: {
			__SGLib__::__kb__::keys[wParam] = 0;
			break;
		}

		default:
			break;
	}
}

void __sgCanvas_OnChar__(WPARAM wParam, LPARAM lParam)
{
	SG_ON_WINDOWFAIL
		return;

	INPUT_RECORD evt = {0};

	evt.EventType                        = KEY_EVENT;
	evt.Event.KeyEvent.bKeyDown          = true;
	evt.Event.KeyEvent.wRepeatCount      = 1;
	evt.Event.KeyEvent.uChar.AsciiChar   = (unsigned char)(wParam);
	evt.Event.KeyEvent.wVirtualScanCode  = (unsigned char)(lParam >> 16);
	evt.Event.KeyEvent.wVirtualKeyCode   = (WORD)MapVirtualKey((WORD)(lParam >> 16), 3);
	evt.Event.KeyEvent.dwControlKeyState = 0;

	DWORD written = 0;
	WriteConsoleInput (GetStdHandle (STD_INPUT_HANDLE), &evt, 1, &written);
}

void __sgCanvas_OnMouse__(UINT msg, WPARAM wParam, LPARAM lParam)
{
	SG_ON_WINDOWFAIL
		return;

	switch (msg) {
		case WM_LBUTTONDOWN:
		case WM_LBUTTONUP:
		case WM_MBUTTONDOWN:
		case WM_MBUTTONUP:
		case WM_RBUTTONDOWN:
		case WM_RBUTTONUP:
			__SGLib__::__mouse__::buttons = wParam;
			break;

		case WM_MOUSEMOVE:
			__SGLib__::__mouse__::X = LOWORD(lParam);
			__SGLib__::__mouse__::Y = HIWORD(lParam);
			break;

		case WM_MOUSEWHEEL:
			__SGLib__::__mouse__::wheel += lParam / 120;
			break;

		default:
			break;
	}
}

void __sgCanvas_OnDestroy__()
{
	SG_ON_WINDOWFAIL
		return;

	__sgCleanup__();
	PostQuitMessage(0);
}

void __sgCleanup__()
{
	SG_ON_WINDOWFAIL
		return;

	__SGLib__::__window__::created = 0;
	__SGLib__::__window__::descriptor = 0;
}

void __sgExit__()
{
	char title[1024] = "";
	GetWindowText(sgWindow(), title, sizeof(title) - 1);

	sprintf(title, "%s - [Ended]", title);
	SetWindowText(sgWindow(), title);

	while (!GetAsyncKeyState(VK_ESCAPE) && sgOK())
		Sleep(0);

	ShowWindow(GetConsoleWindow(), SW_SHOW);
	SetForegroundWindow(GetConsoleWindow());
}

HDC __sgGenBuffer__(HWND window)
{
	HDC parent = GetDC(window);

	if (!parent)
		return 0;

	RECT rect = {0};
	if (window)
		GetClientRect(window, &rect) asserted;

	SIZE size = {rect.right - rect.left, rect.bottom - rect.top};

	HDC dc = Windows::CreateCompatibleDC(parent);
	if (!dc)
		return 0;

	HBITMAP bmap = Windows::CreateCompatibleBitmap(parent, size.cx, size.cy);
	if (!bmap)
		return 0;

	Windows::SelectObject(dc, bmap) asserted;
	ReleaseDC(window, parent) asserted;

	return dc;
}

void __sgConsole_Draw__()
{
	SG_ON_WINDOWFAIL
		return;

	HDC dc = __SGLib__::__window__::buffer[BUFFER_CON];

	HANDLE out = GetStdHandle (STD_OUTPUT_HANDLE);

	CONSOLE_SCREEN_BUFFER_INFO con = {{0}};
	if (!GetConsoleScreenBufferInfo(out, &con))
		return;

	POINT size = {con.srWindow.Right  - con.srWindow.Left + 1,
	              con.srWindow.Bottom - con.srWindow.Top  + 1};

	SIZE font_size = { 12, 16 };
	Windows::GetTextExtentPoint32(dc, "W", 1, &font_size) asserted;

	COLORREF pal[16] = {0x000000, 0x800000, 0x008000, 0x808000, 0x000080, 0x800080, 0x008080, 0xC0C0C0,
                            0x808080, 0xFF0000, 0x00FF00, 0xFFFF00, 0x0000FF, 0xFF00FF, 0x00FFFF, 0xFFFFFF };

	for (int y = 0; y < size.y; y++) {
		char chr[1025] = "";
		WORD atr[1025] = {0};
		COORD coord = {(short)(con.srWindow.Left), (short)(y + con.srWindow.Top)};
		DWORD read  = 0;

		if (!ReadConsoleOutputCharacter(out, chr, ArraySize(chr) - 1, coord, &read)) continue;
		if (!ReadConsoleOutputAttribute(out, atr, ArraySize(atr) - 1, coord, &read)) continue;

		for (int x = 0, xEnd = size.x; x < size.x; x = xEnd) {
			Windows::SetTextColor(dc, pal[atr[x] & 0x0F]);
			Windows::SetBkColor(dc, pal[(atr[x] >> 4) & 0x0F]);
			Windows::SetBkMode(dc, (atr[x] & 0xF0)?(OPAQUE):(TRANSPARENT));

			for(xEnd = x + 1; atr[xEnd] == atr[x] && xEnd < size.x; xEnd++);

			Windows::TextOut(dc, font_size.cx * (x + con.srWindow.Left),
			                 font_size.cy * y, chr + x, xEnd - x) asserted;
		}
        }

	Windows::SetTextColor(dc, pal[con.wAttributes & 0x0F]);
	Windows::SetBkColor(dc, pal[(con.wAttributes >> 4) & 0x0F]);
	Windows::SetBkMode(dc, TRANSPARENT);

	if (__SGLib__::__user__::cursor)
		Windows::TextOut(__SGLib__::__window__::buffer[BUFFER_CON],
	        	   	 font_size.cx * (con.dwCursorPosition.X - con.srWindow.Left),
		   		 font_size.cy * (con.dwCursorPosition.Y - con.srWindow.Top) + 1,
	   	 	      	 "_", 1) asserted;

}

inline int sgOK()
{
	return __SGLib__::__window__::created && sgWindow();
}

inline void sgCreateWindow(int width, int height, const char title[])
{
	!sgOK() asserted;

	__SGLib__::__window__::width = width;
	__SGLib__::__window__::height = height;
	__SGLib__::__window__::title = title;

	__SGLib__::__window__::proc = (HANDLE)_beginthread((void (*)(void *))__sgCanvas_CreateWindow__, 0, 0);

	int begin = clock();
	while (begin + SG_TIMEOUT > clock() && !sgOK())
		Sleep(0);

	Sleep(40);

	sgOK() asserted;

	atexit(__sgExit__);
}

inline HDC& sgBuffer()
{
	return __SGLib__::__window__::buffer[BUFFER_DRAW];
}

inline HWND& sgWindow()
{
	return __SGLib__::__window__::descriptor;
}

inline SIZE sgGetExtent()
{
	SG_ON_WINDOWFAIL
		return {0, 0};

	return {__SGLib__::__window__::width, __SGLib__::__window__::height};
}

inline int sgGetExtentX()
{
	SG_ON_WINDOWFAIL
		return 0;

	return sgGetExtent().cx;
}

inline int sgGetExtentY()
{
	SG_ON_WINDOWFAIL
		return 0;

	return sgGetExtent().cy;
}

inline POINT sgMousePos()
{
	SG_ON_WINDOWFAIL
		return {0, 0};

	return {__SGLib__::__mouse__::X, __SGLib__::__mouse__::Y};
}

inline int sgMouseX()
{
	SG_ON_WINDOWFAIL
		return 0;

	return sgMousePos().x;
}

inline int sgMouseY()
{
	SG_ON_WINDOWFAIL
		return 0;

	return sgMousePos().y;
}

inline int sgMouseWheel()
{
	SG_ON_WINDOWFAIL
		return 0;

	return __SGLib__::__mouse__::wheel;
}

inline int sgMouseButtons()
{
	SG_ON_WINDOWFAIL
		return 0;

	return __SGLib__::__mouse__::buttons;
}

inline void sgClearColor(COLORREF color /* = sgGetFillColor()*/)
{
	SG_ON_WINDOWFAIL
		return;

	COLORREF last = sgGetFillColor();

	sgSetFillColor(color);
	PatBlt(sgBuffer(), 0, 0, sgGetExtentX(), sgGetExtentY(), PATCOPY);
	sgSetFillColor(last);
}

inline void sgSetColor(COLORREF color, int thikness /* = 1*/)
{
	SG_ON_WINDOWFAIL
		return;

	Windows::SelectObject(sgBuffer(), (color != SG_NULL)?(Windows::CreatePen(PS_SOLID, thikness, color)):(Windows::GetStockObject(NULL_PEN)));
	Windows::SetTextColor(sgBuffer(), color);
	Windows::SetTextColor(__SGLib__::__window__::buffer[BUFFER_CON], color);
}

inline COLORREF sgGetColor()
{
	SG_ON_WINDOWFAIL
		return CLR_INVALID;

	HGDIOBJ obj = Windows::GetCurrentObject(sgBuffer(), OBJ_PEN);
	assert (obj);
	if (!obj)
		return CLR_INVALID;

	union {
		EXTLOGPEN extLogPen;
		LOGPEN LogPen;
	} buf = {{0}};

	int size = Windows::GetObject(obj, 0, NULL);
	Windows::GetObject(obj, sizeof(buf), &buf) asserted;

	return (size == sizeof(LOGPEN))?(buf.LogPen.lopnColor):(buf.extLogPen.elpColor);
}

inline void sgSetFillColor(COLORREF color)
{
	SG_ON_WINDOWFAIL
		return;

	Windows::SelectObject(sgBuffer(), (color != SG_NULL)?(Windows::CreateSolidBrush(color)):(Windows::GetStockObject(NULL_BRUSH)));
}

inline COLORREF sgGetFillColor()
{
	SG_ON_WINDOWFAIL
		return CLR_INVALID;

	HGDIOBJ obj = GetCurrentObject(sgBuffer(), OBJ_BRUSH);
	assert (obj);
	if (!obj)
		return CLR_INVALID;

	LOGBRUSH buf = {0};
	GetObject(obj, sizeof(buf), &buf) asserted;

	return buf.lbColor;
}

inline void sgSetPixel(int x, int y, COLORREF color /*= sgGetColor()*/)
{
	SG_ON_WINDOWFAIL
		return;

	sgDraw(Windows::SetPixel(sgBuffer(), x, y, color));
}

inline COLORREF sgGetPixel(int x, int y)
{
	SG_ON_WINDOWFAIL
		return CLR_INVALID;

	return Windows::GetPixel(sgBuffer(), x, y);
}

inline void sgLine(int x0, int y0, int x1, int y1)
{
	SG_ON_WINDOWFAIL
		return;

	sgDraw((Windows::MoveToEx(sgBuffer(), x0, y0, 0), Windows::LineTo(sgBuffer(), x1, y1)));
}

inline void sgRectangle(int x0, int y0, int x1, int y1)
{
	SG_ON_WINDOWFAIL
		return;

	sgDraw(Windows::Rectangle(sgBuffer(), x0, y0, x1, y1));
}

inline void sgEllipse(int x0, int y0, int x1, int y1)
{
	SG_ON_WINDOWFAIL
		return;

	sgDraw(Windows::Ellipse(sgBuffer(), x0, y0, x1, y1));
}

inline void sgCircle(int x, int y, int rad)
{
	SG_ON_WINDOWFAIL
		return;

	sgEllipse(x, y, rad, rad);
}

inline void sgRoundRect(int x, int y, int x1, int y1, int dx, int dy)
{
	SG_ON_WINDOWFAIL
		return;

	sgDraw(Windows::RoundRect(sgBuffer(), x, y, x1, y1, dx, dy));
}

inline void sgArc(int x0, int y0, int x1, int y1, int startAngle, int totalAngle)
{
	SG_ON_WINDOWFAIL
		return;

	POINT center = {(x0 + x1) / 2, (y0 + y1) /2};

	double start =  startAngle               * M_PI / 180,
	       end   = (startAngle + totalAngle) * M_PI / 180;

	sgDraw(Windows::Arc(sgBuffer(), x0, y0, x1, y1,
	                    (int)(center.x + 1E3 * cos(start)), (int)(center.y - 1E3 * sin(start)),
	                    (int)(center.x + 1E3 * cos(end)),   (int)(center.y - 1E3 * sin(end))));
}

inline void sgPie(int x0, int y0, int x1, int y1, int startAngle, int totalAngle)
{
	SG_ON_WINDOWFAIL
		return;

	POINT center = {(x0 + x1) / 2, (y0 + y1) /2};

	double start =  startAngle               * M_PI / 180,
	       end   = (startAngle + totalAngle) * M_PI / 180;

	sgDraw(Windows::Pie(sgBuffer(), x0, y0, x1, y1,
	                    (int)(center.x + 1E3 * cos(start)), (int)(center.y - 1E3 * sin(start)),
	                    (int)(center.x + 1E3 * cos(end)),   (int)(center.y - 1E3 * sin(end))));

}

inline void sgChord(int x0, int y0, int x1, int y1, int startAngle, int totalAngle)
{
	SG_ON_WINDOWFAIL
		return;

	POINT center = {(x0 + x1) / 2, (y0 + y1) /2};

	double start =  startAngle               * M_PI / 180,
	       end   = (startAngle + totalAngle) * M_PI / 180;

	sgDraw(Windows::Chord(sgBuffer(), x0, y0, x1, y1,
		              (int)(center.x + 1E3 * cos(start)), (int)(center.y - 1E3 * sin(start)),
	                      (int)(center.x + 1E3 * cos(end)),   (int)(center.y - 1E3 * sin(end))));
}

inline void sgPolygon(POINT *vertices, int num_vertices /*= ~0*/)
{
	SG_ON_WINDOWFAIL
		return;

	if (!vertices)
		return;

	if (num_vertices == ~0)
		num_vertices = ArraySize(vertices);

	sgDraw(Windows::Polygon(sgBuffer(), vertices, num_vertices));
}

inline void sgPolyline(POINT *vertices, int num_vertices /*= ~0*/)
{
	SG_ON_WINDOWFAIL
		return;

	if (!vertices)
		return;

	if (num_vertices == ~0)
		num_vertices = ArraySize(vertices);

	sgDraw(Windows::Polyline(sgBuffer(), vertices, num_vertices));
}

inline void sgTextOut(int x, int y, char *text)
{
	SG_ON_WINDOWFAIL
		return;

	sgDraw(Windows::TextOut(sgBuffer(), x, y, text, strlen(text)));
}

inline void sgSetConsoleAttribute(unsigned short attr)
{
	SG_ON_WINDOWFAIL
		return;

	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), attr);
}

inline void sgClearConsole()
{
	SG_ON_WINDOWFAIL
		return;

	HANDLE out = GetStdHandle(STD_OUTPUT_HANDLE);

	CONSOLE_SCREEN_BUFFER_INFO con = {{0}};
	GetConsoleScreenBufferInfo (out, &con) asserted;

	COORD start = {con.srWindow.Left, con.srWindow.Top};

	DWORD len   = (con.srWindow.Right  - con.srWindow.Left + 1) *
	              (con.srWindow.Bottom - con.srWindow.Top  + 1);

	DWORD written = 0;
	FillConsoleOutputCharacter(out, ' ',    len, start, &written) asserted;
	FillConsoleOutputAttribute(out, con.wAttributes, len, start, &written) asserted;

	SetConsoleCursorPosition(GetStdHandle (STD_OUTPUT_HANDLE), start) asserted;
}

inline void sgCursor(int val /*= 0*/)
{
	SG_ON_WINDOWFAIL
		return;

	__SGLib__::__user__::cursor = val;
}

inline void sgPlaySound(LPCSTR path, DWORD mode /*= SND_FILENAME*/)
{
	Windows::PlaySound(path, 0, mode);
}

inline int sgLock(bool force /*= true*/)
{
	SG_ON_WINDOWFAIL
		return 0;

	return ((force)?(EnterCriticalSection(&__SGLib__::__window__::cs), 1):
		            (TryEnterCriticalSection(&__SGLib__::__window__::cs)));
}

inline void sgUnlock()
{
	SG_ON_WINDOWFAIL
		return;

	LeaveCriticalSection(&__SGLib__::__window__::cs);
}

template<typename TYPE>
inline TYPE sgUnlock(TYPE val)
{
	SG_ON_WINDOWFAIL
		return val;

	sgUnlock();
	return val;
}

#define $FBLUE sgSetConsoleAttribute(FOREGROUND_BLUE)
#define $FGREEN sgSetConsoleAttribute(FOREGROUND_GREEN)
#define $FRED sgSetConsoleAttribute(FOREGROUND_RED)
#define $FINTENSITY sgSetConsoleAttribute(FOREGROUND_INTENSITY)
#define $BBLUE sgSetConsoleAttribute(BACKGROUND_BLUE)
#define $BGREEN sgSetConsoleAttribute(BACKGROUND_GREEN)
#define $BINTENSITY sgSetConsoleAttribute(BACKGROUND_INTENSITY)

#endif // SGLIB_H
